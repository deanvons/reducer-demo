import { useReducer, useState } from "react";

function ReducerDemo(props) {
  const [guitars, dispatch] = useReducer(reducer, []);

  function reducer(guitars, action) {
    switch (action.type) {
      case "add-default":
        return [...guitars, { model: "default" }];
      case "add-guitar":
        return [...guitars, action.payload];
      default:
        return guitars;
    }
  }

  function handleAddDefault() {
    console.log(guitars);
    dispatch(AddDefault());
  }

  function handleAddNew() {
    console.log(guitars);
    dispatch({
      type: "add-guitar",
      payload: { model: "newly created guitar" },
    });
  }

  function AddDefault() {
    return { type: "add-default" };
  }

  return (
    <div>
      <h1>Reducer Demo</h1>
      <button onClick={handleAddDefault}>Add Default</button>
      <button onClick={handleAddNew}>Add New</button>
    </div>
  );
}

export default ReducerDemo;
