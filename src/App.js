import logo from './logo.svg';
import './App.css';
import ReducerDemo from './Components/ReducerDemo';

function App() {
  return (
    <div className="App">
 <ReducerDemo/>
    </div>
  );
}

export default App;
